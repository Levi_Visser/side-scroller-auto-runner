﻿using System;
using UnityEngine.Events;

[Serializable]
public class PlayerFinishedEvent : UnityEvent<PlayerController> {}