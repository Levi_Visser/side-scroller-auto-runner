﻿using Assets._Code.Base;
using Com.LuisPedroFonseca.ProCamera2D;
using System;
using UnityEngine;

partial class GameSettings
{
    public CameraSettings CameraSettings;
}

[Serializable]
public class CameraSettings
{
    public float DirectionOffset = 1;
}

namespace Assets._Code.Gameplay
{
    public class CameraController : GameBehaviour
    {
        private CameraSettings settings;
        public ProCamera2D procamera2D;
        public ProCamera2DNumericBoundaries cameraBoundries;

        protected override void OnInitialize()
        {
            settings = GameSettings.CameraSettings;

            SetTarget(GameController.GameplayManager.GetLocalPlayerController().transform);
        }

        public void SetTarget(Transform _target)
        {
            procamera2D.AddCameraTarget(_target);
        }

        public void SetOffset(EDirection dir)
        {
            procamera2D.OffsetX = (int)dir * settings.DirectionOffset;
        }
    }
}
