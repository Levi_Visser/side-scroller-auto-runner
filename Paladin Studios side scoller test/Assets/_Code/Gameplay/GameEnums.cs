﻿public enum EPlayerControlType
{
    NULL = 0,
    Player = 1,
    AI = 2,
}

public enum EDirection
{
    Right = 1,
    Standing = 0,
    Left = -1
}
