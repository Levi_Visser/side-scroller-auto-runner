﻿using Assets._Code.Base;
using UnityEngine;
using UnityEngine.UI;

namespace Assets._Code.Gameplay.UI
{
    public class GameOverUIController : GameBehaviour
    {
        [SerializeField] private Button restart;
        protected override void OnInitialize()
        {
            restart.onClick.AddListener(delegate
            {
                GameController.Restart();
            });

            gameObject.SetActive(false);
        }
    }
}
