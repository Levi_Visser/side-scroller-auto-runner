﻿using Assets._Code.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace Assets._Code.Gameplay.UI
{
    public class LapCounterUIController : GameBehaviour
    {
        [SerializeField] private TextMeshProUGUI lapText;

        protected override void OnInitialize()
        {
            gameObject.SetActive(true);
            ResetNumberOfLaps();
        }

        public void UpdateNumerOfLaps(int current, int goal)
        {
            lapText.text = "Laps " + current + "/" + goal;
        }

        public void UpdatePlayerFinished(PlayerController playerController)
        {
            // In case of multiple (AI) players, specific UI elements based on a specific player could be updated
            lapText.text = "You Finished!";
        }

        private void ResetNumberOfLaps()
        {
            UpdateNumerOfLaps(0, GameController.GameSettings.LevelSettings.Levels[0].LapsToWin);
        }
    }
}
