﻿using Assets._Code.Base;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Assets._Code.Gameplay.UI
{
    public class HealthUIController : GameBehaviour
    {
        [SerializeField] private GameObject hearthObjectPrefab;
        [SerializeField] private Transform heartContainer;
        private List<Image> Hearts = new List<Image>();

        protected override void OnInitialize()
        {
            gameObject.SetActive(true);
            InitializeHarts();
        }

        private void InitializeHarts()
        {
            if (Hearts.Count > 0)
            {
                for (int i = 0; i < Hearts.Count; i++)
                {
                    Destroy(Hearts[i].gameObject);
                }
                Hearts.Clear();
            }
            for (int i = 0; i < GameSettings.PlayerSettings.Health; i++)
            {
                Image img = Instantiate(hearthObjectPrefab, heartContainer).GetComponent<Image>();
                Hearts.Add(img);
            }
        }

        public void UpdateUI(int currentHealth)
        {
            if (currentHealth >= 0)
            {
                for (int i = 0; i <= Hearts.Count; i++)
                {
                    if (i > currentHealth)
                    {
                        Hearts[i - 1].DOFade(0.1f, 0.5f);
                    }
                }
            }
        }
    }
}
