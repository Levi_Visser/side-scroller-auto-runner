﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public partial class GameSettings
{
    public LevelSettings LevelSettings;
}


[Serializable]
public class LevelSettings
{
    // Used in multiple places where level index 0 is hardcoded. When more levels would be added, that needs to be replaced with a level system
    public List<LevelProperties> Levels = new List<LevelProperties>();
}


[Serializable]
public struct LevelProperties
{
    public int LapsToWin;
}
