﻿using Assets._Code.Base;
using System.Collections.Generic;

namespace Assets._Code.Gameplay.Managers
{
    public class GameplayManager : GameBehaviour
    {
        public List<PlayerController> PlayerControllers = new List<PlayerController>();

        protected override void OnInitialize()
        {
            for (int i = 0; i < PlayerControllers.Count; i++)
            {
                if (PlayerControllers[i] == null)
                {
                    continue;
                }
                PlayerControllers[i].Initialize(GameController);
            }
        }

        internal override void HandleUpdate()
        {
            for (int i = 0; i < PlayerControllers.Count; i++)
            {
                if (PlayerControllers[i] == null)
                {
                    continue;
                }
                PlayerControllers[i].HandleUpdate();
            }
        }

        internal void HandleFixedUpdate()
        {
            for (int i = 0; i < PlayerControllers.Count; i++)
            {
                if (PlayerControllers[i] == null)
                {
                    continue;
                }
                PlayerControllers[i].HandleFixedUpdate();
            }
        }

        internal void HandleLateUpdate()
        {
            for (int i = 0; i < PlayerControllers.Count; i++)
            {
                if (PlayerControllers[i] == null)
                {
                    continue;
                }
                PlayerControllers[i].HandleLateUpdate();
            }
        }

        public PlayerController GetLocalPlayerController()
        {
            return PlayerControllers[0];
        }
    }
}
