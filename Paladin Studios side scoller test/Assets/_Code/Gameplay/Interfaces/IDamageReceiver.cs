﻿
namespace Assets._Code.Gameplay.Interfaces
{
    public interface IDamageReceiver
    {
        void TakeDamage(int amount);
    }
}
