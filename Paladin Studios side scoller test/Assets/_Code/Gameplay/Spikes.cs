﻿using Assets._Code.Base;
using Assets._Code.Gameplay.Interfaces;
using System;
using UnityEngine;

public partial class GameSettings
{
    public SpikeSettings SpikeSettigs;
}

[Serializable] 
public class SpikeSettings
{
    public int DamageAmount;
    public float JumpMultiplier;
}

public class Spikes : GameBehaviour
{
    protected override void OnInitialize()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        IDamageReceiver damagableObj = collision.gameObject.GetComponent<IDamageReceiver>();
        if (damagableObj != null)
        {
            damagableObj.TakeDamage(GameController.GameSettings.SpikeSettigs.DamageAmount);
        }
    }
}
