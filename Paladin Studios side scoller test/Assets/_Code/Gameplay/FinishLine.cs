﻿using Assets._Code.Base;
using UnityEngine;

public class FinishLine : GameBehaviour
{
    public PlayerFinishedEvent PlayerFinished;

    protected override void OnInitialize()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            PlayerController _playerController = collision.GetComponent<PlayerController>();
            if (_playerController != null && _playerController.Direction == EDirection.Right)
            {
                _playerController.AddLap();
                if(_playerController.ControlType == EPlayerControlType.Player)
                {
                    GameController.LapCounterUIController.UpdateNumerOfLaps(_playerController.LapsCompleted, GameController.GameSettings.LevelSettings.Levels[0].LapsToWin);
                }
                if (_playerController.HasPlayerFinished())
                {
                    HandlePlayerFinish(_playerController);
                }

                if(_playerController.LapsCompleted == GameController.GameSettings.LevelSettings.Levels[0].LapsToWin)
                {
                    GameController.GameWon();
                }
            }
        }
    }

    private void HandlePlayerFinish(PlayerController playerController)
    {
        PlayerFinished.Invoke(playerController);
    }
}
