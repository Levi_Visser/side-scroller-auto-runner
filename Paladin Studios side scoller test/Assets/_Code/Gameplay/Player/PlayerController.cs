﻿using Assets._Code.Base;
using Assets._Code.Gameplay;
using Assets._Code.Gameplay.Interfaces;
using System;
using System.Collections;
using UnityEngine;

public partial class GameSettings
{
    public SettingsPlayer PlayerSettings;
}

[Serializable]
public class SettingsPlayer
{
    [Header("Movement")]
    public float Speed = 2.0f;
    public float MovementSmoothStep = 0.1f;

    [Header("Jump")]
    public float JumpSpeed = 5.0f;
    public float GravityScale = 5.0f;

    [Header("Sliding")]
    public float SlidingSpeed = -0.5f;
    public float SlidingSmoothStep = 0.1f;

    [Header("Health")]
    public int Health = 3;
    public Color NormalColor;
    public Color DamagedColor;
    public int SpriteFlashTime = 3;

    [Header("StartPos")]
    public Vector2 StartPos;
}

[Serializable]
public class Components
{
    public Rigidbody2D rb;
    public Collider2D col;
    public Transform modelHolder;
    public OverlapCheck groundCheck;
    public OverlapCheck wallCheck;
    public SpriteRenderer spriteRenderer;
}

public partial class PlayerController : GameBehaviour, IDamageReceiver
{
    public EPlayerControlType ControlType { get; private set; }
    public int LapsCompleted { get; private set; }
    public bool HasFinished;


    [SerializeField] private Components Components;
    private SettingsPlayer settings;

    public EDirection Direction
    {
        get { return dir; }
    }
    private EDirection dir = EDirection.Standing;

    public int Health
    {
        get { return currentHealth; }
        set { currentHealth = value; }
    }
    private int currentHealth;

    private bool IsGrounded
    {
        get { return Components.groundCheck.isOverlapping; }
    }
    private bool IsTouchingWall
    {
        get { return Components.wallCheck.isOverlapping; }
    }

    private Vector2 targetVelocity;

    private bool shouldJump;
    private float gravityScale;
    private float slidingSpeed;

    private float hSpeed;
    private float hTargetSpeed;
    private bool shouldFlashSprite;

    protected override void OnInitialize()
    {
        settings = GameSettings.PlayerSettings;
        gravityScale = Components.rb.gravityScale = settings.GravityScale;

        ControlType = EPlayerControlType.Player;
        currentHealth = settings.Health;

        transform.position = settings.StartPos;
        dir = EDirection.Standing;
        LapsCompleted = 0;
    }

    internal override void HandleUpdate()
    {
        if (HasFinished) { return; }

        HandleLocalPlayerInput();
    }

    internal void HandleFixedUpdate()
    {
        if (HasFinished)
        {
            Components.rb.velocity = new Vector2(0, Components.rb.velocity.y);
            return;
        }

        targetVelocity = Components.rb.velocity;
        HandleMovement();
        HandleSliding();
        HandleJumping();
        Components.rb.velocity = targetVelocity;
    }

    internal void HandleLateUpdate()
    {
    }

    private void HandleMovement()
    {
        hSpeed = Mathf.SmoothStep(hSpeed, hTargetSpeed, settings.MovementSmoothStep);
        targetVelocity.x = hSpeed;
    }

    private void HandleSliding()
    {
        if (IsTouchingWall && !IsGrounded)
        {
            hSpeed = 0;
            if (Components.rb.velocity.y < 0)
            {
                slidingSpeed = Mathf.SmoothStep(slidingSpeed, settings.SlidingSpeed, settings.SlidingSmoothStep);
                Components.rb.gravityScale = 0;
                targetVelocity.y = slidingSpeed;
            }
        }
        else
        {
            slidingSpeed = 0f;
            Components.rb.gravityScale = gravityScale;
        }
    }

    private void HandleJumping()
    {
        if (shouldJump)
        {
            shouldJump = false;
            if (IsTouchingWall)
            {
                hSpeed = hTargetSpeed;
            }
            targetVelocity.y = settings.JumpSpeed;
        }
    }

    private void HandleLocalPlayerInput()
    {
        if (dir == EDirection.Standing)
        {
            GiveInput();
        }

        if (Input.GetMouseButtonDown(0))
        {
            GiveInput();
        }
    }

    private void GiveInput()
    {
        if (dir == EDirection.Standing)
        {
            SetDir(EDirection.Right, true);
        }
        else if (IsGrounded || IsTouchingWall)
        {
            shouldJump = true;
        }

        if (IsTouchingWall && !IsGrounded)
        {
            InverseDirection(false);
        }
    }

    private void InverseDirection(bool _smooth)
    {
        switch (dir)
        {
            case EDirection.Right:
                SetDir(EDirection.Left, _smooth);
                break;
            case EDirection.Left:
                SetDir(EDirection.Right, _smooth);
                break;
        }
    }

    private void SetDir(EDirection _dir, bool _smooth)
    {
        dir = _dir;

        if (ControlType == EPlayerControlType.Player)
        {
            GameController.CameraController.SetOffset(dir);
        }

        switch (dir)
        {
            case EDirection.Right:
                Components.modelHolder.localScale = new Vector3(1.0f, 1.0f, 1.0f);
                break;
            case EDirection.Left:
                Components.modelHolder.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
                break;
        }

        var _tSpeed = (int)dir * settings.Speed;
        if (_smooth)
        {
            hTargetSpeed = _tSpeed;
        }
        else
        {
            hSpeed = hTargetSpeed = _tSpeed;
        }
    }

    public void AddLap()
    {
        LapsCompleted++;
        Debug.LogFormat("Playercontroller: {0} has finished a lap. Total finished laps: {1}", gameObject, LapsCompleted);
    }

    public bool HasPlayerFinished()
    {
        int LapsToComplete = GameController.GameSettings.LevelSettings.Levels[0].LapsToWin;
        HasFinished = LapsCompleted == LapsToComplete;
        return HasFinished;
    }

    void IDamageReceiver.TakeDamage(int amount)
    {
        currentHealth -= amount;
        if (ControlType == EPlayerControlType.Player)
        {
            GameController.HealthUIController.UpdateUI(currentHealth);
        }

        shouldFlashSprite = true;
        StartCoroutine(FlashPlayerColor());
        StartCoroutine(FlashSpriteCounter());

        targetVelocity.y = settings.JumpSpeed * GameController.GameSettings.SpikeSettigs.JumpMultiplier;
        Components.rb.velocity = targetVelocity;

        if (currentHealth <= 0)
        {
            GameController.GameOver();
        }
    }

    private IEnumerator FlashPlayerColor()
    {
        while (shouldFlashSprite)
        {
            Components.spriteRenderer.color = settings.DamagedColor;
            yield return new WaitForSeconds(0.15f);
            Components.spriteRenderer.color = settings.NormalColor;
            yield return new WaitForSeconds(0.15f);
        }
    }

    private IEnumerator FlashSpriteCounter()
    {
        yield return new WaitForSeconds(settings.SpriteFlashTime);
        shouldFlashSprite = false;
        StopCoroutine(FlashPlayerColor());
    }
}
