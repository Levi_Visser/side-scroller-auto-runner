﻿using UnityEngine;

namespace Assets._Code.Gameplay
{
    public class OverlapCheck : MonoBehaviour
    {
        public bool isOverlapping = false;

        private void OnTriggerStay2D(Collider2D collision)
        {
            isOverlapping = true;
        }
        private void OnTriggerExit2D(Collider2D collision)
        {
            isOverlapping = false;
        }
    }
}
