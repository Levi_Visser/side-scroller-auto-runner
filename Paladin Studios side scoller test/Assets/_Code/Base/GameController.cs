﻿using Assets._Code.Gameplay;
using Assets._Code.Gameplay.Managers;
using Assets._Code.Gameplay.UI;
using System.Collections.Generic;
using UnityEngine;

namespace Assets._Code.Base
{
    public class GameController : MonoBehaviour
    {
        public GameplayManager GameplayManager;
        public CameraController CameraController;
        public GameSettings GameSettings;

        /// <summary>
        /// Ideally there was 1 full UI Controller object owning every UI in the game
        /// Instead of initializing individual UI parts here, I would only initialize the Upper UI Controller, which would take care of the rest
        /// </summary>
        public LapCounterUIController LapCounterUIController;
        public HealthUIController HealthUIController;
        public GameOverUIController GameOverUIController;
        public GameOverUIController GameWonUIController;

        /// <summary>
        /// Ideally there would have been a level spawner
        /// This spawner whould have taken care of the spawning and initialization levels
        /// which, in their place, have a fisnih line and spikes as child and thus take care of the initialization of those objects
        /// </summary>
        public FinishLine FinishLine;
        public List<Spikes> Spikes = new List<Spikes>();

        private bool isGameOver;

        private void Awake()
        {
            AddEventListsners();
        }

        private void Start()
        {
            Restart();
        }

        private void Update()
        {
            if(isGameOver) { return; }
            GameplayManager.HandleUpdate();
        }

        private void FixedUpdate()
        {
            if (isGameOver) { return; }
            GameplayManager.HandleFixedUpdate();
        }

        private void LateUpdate()
        {
            if (isGameOver) { return; }
            GameplayManager.HandleLateUpdate();
        }

        private void AddEventListsners()
        {
            FinishLine.PlayerFinished.AddListener(LapCounterUIController.UpdatePlayerFinished);
        }

        public void Restart()
        {
            isGameOver = false;
            GameplayManager.Initialize(this);
            CameraController.Initialize(this);

            LapCounterUIController.Initialize(this);
            HealthUIController.Initialize(this);
            GameOverUIController.Initialize(this);
            GameWonUIController.Initialize(this);

            FinishLine.Initialize(this);

            for(int i = 0; i < Spikes.Count; i++)
            {
                Spikes[i].Initialize(this);
            }
        }

        public void GameOver()
        {
            isGameOver = true;
            LapCounterUIController.gameObject.SetActive(false);
            HealthUIController.gameObject.SetActive(false);
            GameOverUIController.gameObject.SetActive(true);
        }

        public void GameWon()
        {
            isGameOver = true;
            LapCounterUIController.gameObject.SetActive(false);
            HealthUIController.gameObject.SetActive(false);
            GameWonUIController.gameObject.SetActive(true);
        }
    }
}
