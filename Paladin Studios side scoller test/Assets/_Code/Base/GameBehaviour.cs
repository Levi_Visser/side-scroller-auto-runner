﻿using Assets._Code.Gameplay;
using UnityEngine;

namespace Assets._Code.Base
{
    public abstract class GameBehaviour : MonoBehaviour
    {
        public virtual Vector3 Position { get { return transform.position; } set { transform.position = value; } }
        protected GameSettings GameSettings { get { return GameController.GameSettings; } }

        protected GameController GameController;

        internal virtual void Initialize(GameBehaviour parent)
        {
            Initialize(parent.GameController);
        }

        internal void Initialize(GameController controller)
        {
            GameController = controller;
            OnInitialize();
        }

        /// <summary>
        /// Hook up your controllers in GameController Update function.
        /// For non-controllers or sub-controllers:
        /// Implement this function and call it from your controllers
        /// 
        /// This ensures proper propagation without having random order of execution problems
        /// </summary>
        internal virtual void HandleUpdate() { }

        /// <summary>
        /// Called from Initialize, after GameController is known
        /// </summary>
        protected abstract void OnInitialize();
    }
}
