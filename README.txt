For this test assignment I made a small auto runner game with some platforming elements.
You autorun across the level and you only have to jump with the left mouse button. Jumping from a wall changes your run direction.

- What did I do to improve the game feel?
To improve the game feel I spent the most time on the player movement, the way jumping and sliding from walls work.
To be able to test/change this quickly I made a game object in the scene containing different settings which affect
how quick you run and jump etc.

Also did I implement a really subtile/small running animation with the provided assets and and sprite flashing
functionallity for when you hit an obstacle. 

In my opinion, for a platformer, visual feedback adds a lot to the feel of the game. To further improve the feel of the game
I would implement multiple visual improvements like particles and better/more animations. But this is something which
could not be done in the given time. Getting the gameplay functional is always the first prio for me.
I would also look into creating a better level, with better platforms/obstacles. Right now there might be situations where you get stuck under a platform and as a result, die. Things like these could/should be fixed with proper level design and tweaking of all the parameters.

- Time spent
My total time spent is around 9 hours.
I probably could have done is faster, but since I also wanted to build a base from which I could easilly expand the project
(think about adding an AI to race against for example) I took a bit more time.

- Do I think my code could be improved?
yes, there is always room for improvement. In my code for this project there are some parts which I wanted to do differently/
would have done differently in a larger project.
The way I prefer to work (right now) is having full controll over the execution order/update loop in a game. Right now I accompish this by implementing a single game controller which has the abillity to initialize and update all other objects in the game which need initialization/updating.

Things I would like to improve has mostly to do with the GameController. Right now it has references to individual UI screens, I'd rather have a separate UI manager for that, to keep UI and game logic separated from each other. 

The same goes for level logic. Right now the GameController knows about level specific objects, i.e. the finish line and spikes.
preferably, this logic would go into it's own separate level object, which would be instantiated via some kind of level spawner (adding the functionallity for multiple levels).

In terms of gameplay code improvement, I would like to improve the way the upwards bounce works when you hit spikes. Right now it just adds to y velocity of the player. I thik with more time, a better implementation/designed mechanic could be developed, which would also improve the feel of the game.

Other than that, I would like to refactor the code and fix naming convention issues.

- Third-party code/tools
I used Pro Camera 2D for the follow camera of the player and I used DoTween for a small fade animation in the health UI
